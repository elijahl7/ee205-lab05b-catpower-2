///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: The number that we want to convert from
///    fromUnit:  The energy unit of fromValue
///    toUnit:    The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///   This program will only compile in C++ (with gpp) not in C (with gcc)
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date   15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "joule.h"
#include "ev.h"
#include "megaton.h"
#include "gge.h"
#include "foe.h"
#include "cat.h"

int main(int argc, char *argv[])
{
   double fromValue;
   char fromUnit;
   char toUnit;

   fromValue = atof(argv[1]);
   fromUnit = argv[2][0]; // Get the first character from the second argument
   toUnit = argv[3][0];   // Get the first character from the thrid argument

   // printf( "fromValue = [%lG]\n", fromValue );
   // printf( "fromUnit = [%c]\n", fromUnit );
   // printf( "toUnit = [%c]\n", toUnit );

   double commonValue;
   switch (fromUnit)
   {
   case JOULE:
      commonValue = fromValue; // No conversion necessary
      break;
   case ELECTRON_VOLT:
      commonValue = fromElectronVoltsToJoule(fromValue);
      break;
   case MEGATON:
      commonValue = fromMegatonToJoule(fromValue);
      break;
   case GGE:
      commonValue = fromGgeToJoule(fromValue);
      break;
   case FOE:
      commonValue = fromFoeToJoule(fromValue);
      break;
   case CATPOWER:
      commonValue = fromCatPowerToJoule(fromValue);
      break;
   default:
      printf("Unknown fromUnit [%c]\n", fromUnit);
      exit(EXIT_FAILURE);
   }

   // printf( "commonValue = [%lG] joule\n", commonValue );

   double toValue;

   switch (toUnit)
   {
   case JOULE:
      toValue = commonValue; // No conversion necessary
      break;
   case ELECTRON_VOLT:
      toValue = fromJouleToElectronVolts(commonValue);
      break;
   case MEGATON:
      toValue = fromJouleToMegaton(commonValue);
      break;
   case GGE:
      toValue = fromJouleToGge(commonValue);
      break;
   case FOE:
      toValue = fromJouleToFoe(commonValue);
      break;
   case CATPOWER:
      toValue = fromJouleToCatPower(commonValue);
      break;
   default:
      printf("Unknown toUnit [%c]\n", toUnit);
      exit(EXIT_FAILURE);
   }

   printf("%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit);

   return (EXIT_SUCCESS);
}
