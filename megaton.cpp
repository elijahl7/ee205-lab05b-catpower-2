///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "megaton.h"

const double MEGATONS_IN_A_JOULE = 1 / 4.184e15;

double fromMegatonToJoule(double megaton)
{
    return megaton / MEGATONS_IN_A_JOULE;
}

double fromJouleToMegaton(double joule)
{
    return joule * MEGATONS_IN_A_JOULE;
}