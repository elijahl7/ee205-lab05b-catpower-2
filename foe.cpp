///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "foe.h"

const double FOE_IN_A_JOULE = 1 / 1.0e24;

double fromFoeToJoule(double foe)
{
    return foe / FOE_IN_A_JOULE;
}

double fromJouleToFoe(double joule)
{
    return joule * FOE_IN_A_JOULE;
}