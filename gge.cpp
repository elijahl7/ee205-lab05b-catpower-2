///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

const double GGE_IN_A_JOULE = 1 / 1.213e8;

double fromGgeToJoule(double gge)
{
    return gge / GGE_IN_A_JOULE;
}

double fromJouleToGge(double joule)
{
    return joule * GGE_IN_A_JOULE;
}