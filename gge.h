///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

const char GGE = 'g';

extern double fromGgeToJoule(double);

extern double fromJouleToGge(double);