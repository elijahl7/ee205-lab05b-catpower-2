///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "ev.h"

double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;

double fromElectronVoltsToJoule(double electronVolts)
{
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE;
}

double fromJouleToElectronVolts(double joule)
{
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}