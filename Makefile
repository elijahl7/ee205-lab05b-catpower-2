OBJS	= cat.o catPower.o ev.o foe.o gge.o joule.o megaton.o
SOURCE	= cat.cpp, catPower.cpp, ev.cpp, foe.cpp, gge.cpp, joule.cpp, megaton.cpp,
HEADER	= cat.h, ev.h, foe.h, gge.h, joule.h, megaton.h
OUT	= catPower
CC	 = g++
FLAGS	 = -g -c -Wall
LFLAGS	 = 

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)

cat.o: cat.cpp
	$(CC) $(FLAGS) cat.cpp

catPower.o: catPower.cpp
	$(CC) $(FLAGS) catPower.cpp

ev.o: ev.cpp
	$(CC) $(FLAGS) ev.cpp

foe.o: foe.cpp
	$(CC) $(FLAGS) foe.cpp

gge.o: gge.cpp
	$(CC) $(FLAGS) gge.cpp

joule.o: joule.cpp
	$(CC) $(FLAGS) joule.cpp

megaton.o: megaton.cpp
	$(CC) $(FLAGS) megaton.cpp


clean:
	rm -f $(OBJS)