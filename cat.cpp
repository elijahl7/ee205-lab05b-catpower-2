///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Elijah Lopez <elijahl7@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"

const double CATPOWER_IN_A_JOULE = 0; // Cats do no work

double fromCatPowerToJoule(double catPower)
{
   return 0.0; // Cats do no work
}

double fromJouleToCatPower(double joule)
{
   return 0.0; // Cats do no work
}